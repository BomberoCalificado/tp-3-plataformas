using UnityEngine;

public class Disparo : MonoBehaviour
{
    public GameObject prefab; // Prefab a instanciar y disparar
    public Transform puntoDisparo; // GameObject desde donde se disparar� el prefab

    public float fuerzaDisparo = 1000f;

    public void DispararPrefab()
    {
        // Instancia el prefab y lo dispara desde el punto de disparo
        GameObject nuevoPrefab = Instantiate(prefab, puntoDisparo.position, puntoDisparo.rotation);

        // Aplica una fuerza al prefab solo en el eje X
        Rigidbody rb = nuevoPrefab.GetComponent<Rigidbody>();
        rb.AddForce(puntoDisparo.right * fuerzaDisparo);
    }

    void Update()
    {
        // Detecta cu�ndo se presiona la tecla de disparo y llama a la funci�n DispararPrefab()
        if (Input.GetKeyDown(KeyCode.T))
        {
            DispararPrefab();
        }
    }
}