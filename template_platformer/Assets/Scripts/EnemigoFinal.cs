using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoFinal : MonoBehaviour
{
    // RECURSOS PARA BOMBARDEAR
    public GameObject Bomba;
    public GameObject InstanciadorBombas;
    int Horizontal;
    int Vertical;
    public int ContadorBombas;
    // RECURSOS PARA FORMAR SANGUIJUELAS
    public GameObject Bichos;
    public GameObject PuntoBicho;
    public int contadordesanguijuelas;
    // RECURSOS BASICOS
    public float TiempoHabilidad;
    public bool HablidadBomba;
    public bool HabilidadSangujuela;
    public bool SecreoSanguijuela;
    public static bool Alerta;
    public float VidaEnemigoFinal;
    public GameObject Puerta;
    // RECURSOS DE MOVIMIENTO
    public float VelocidadMov;
    public bool Arriba;



    void Start()
    {
        VidaEnemigoFinal = 300;
    }


    void Update()
    {

        MoverInstanciador();

        // CAMBIOS DE HABILIDAD
        if (Alerta == true)
        {
            TiempoHabilidad = TiempoHabilidad + Time.deltaTime;
            Moverse();
        }

        // ACTIVACION DE HABILIDADES
        if (TiempoHabilidad > 0 && TiempoHabilidad < 2)
            HablidadBomba = true;
        else HablidadBomba = false;

        if (HablidadBomba == true && ContadorBombas <= 3)
        {
            for (int i = 0; i <= 3; i++)
            {
                CrearBombas();
                ContadorBombas++;
            }
        }


        if (TiempoHabilidad > 2 && TiempoHabilidad < 4)
            HabilidadSangujuela = true;
        else HabilidadSangujuela = false;
        if (HabilidadSangujuela == true && contadordesanguijuelas == 0)
        {
            for (int i = 0; i <= 2; i++)
            {
                CrearSangijuelas();
                contadordesanguijuelas++;
            }
        }

        if (TiempoHabilidad <= 5)
            Arriba = true;
        if (TiempoHabilidad > 5)
            Arriba = false;

        //REINICIO DE BUCLE DE HABILIDADES
        if (TiempoHabilidad >= 10)
        {
            TiempoHabilidad = 0;
            contadordesanguijuelas = 0;
            ContadorBombas = 0;
        }

        if (VidaEnemigoFinal <= 0)
        {
            Puerta.SetActive(false);
        }

    }


    // INSTANCIADOR DE BOMBAS
    private void CrearBombas()
    {
        Instantiate(Bomba, InstanciadorBombas.transform.position, Quaternion.identity);

    }

    private void MoverInstanciador()
    {
        Horizontal = Random.Range(-44, -10);
        Vertical = Random.Range(21, 28);

        InstanciadorBombas.transform.position = new Vector3(Horizontal, Vertical, 0);
        PuntoBicho.transform.position = new Vector3(Horizontal, Vertical, 0);
    }

    private void CrearSangijuelas()
    {

        Instantiate(Bichos, PuntoBicho.transform.position, Quaternion.identity);


    }

    private void Moverse()
    {
        if (Arriba == true)
        {
            transform.position = transform.position + new Vector3(0, 1, 0) * VelocidadMov * Time.deltaTime;
        }

        if (Arriba == false
            )
        {
            transform.position = transform.position - new Vector3(0, 1, 0) * VelocidadMov * Time.deltaTime;
        }
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("proyectil"))
        {
            VidaEnemigoFinal = VidaEnemigoFinal - 10;
        }

        if (collision.gameObject.CompareTag("Player3"))
        {
            VidaEnemigoFinal = VidaEnemigoFinal - 30;
        }

        if (collision.gameObject.CompareTag("Player0"))
        {
            VidaEnemigoFinal = VidaEnemigoFinal - 15;
        }

        if (collision.gameObject.CompareTag("Player6"))
        {
            VidaEnemigoFinal = VidaEnemigoFinal - 25;
        }


    }
}
