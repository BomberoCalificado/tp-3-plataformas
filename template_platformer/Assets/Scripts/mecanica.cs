using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ebomba : MonoBehaviour

{
       
    public LayerMask aquienpersigo;  // EN QUE CAPA VA DETECTAR OBJETOS
    public float rangovision; // RANGO DE VISION
    public bool alerta;  // ESTA ALERTA O NO?
    public float Velocidadpersecución; // A QUE VELOCIDAD SE MUEVE
    public float vida = 100; // VIDA DEL PERSONAJE
    public GameObject ubicaciondearma;
    public GameObject armamento;
    public int Intruso;
    public float DistanciaRayo;
    public bool Alguienenlaizquierda;
    



    // REFERENCIA DE PLAYERS
    public GameObject Player0;
    public GameObject Player1;
    public GameObject Player2;
    public GameObject Player3;
    public GameObject Player4;
    public GameObject Player5;
    public GameObject Player6;


    void Start()
    {
        alerta = false;

    }


    void Update()
    {


        if (vida <= 0)
        {
            armamento.gameObject.transform.SetParent(null); // SUELTA EL ARMA
            armamento.gameObject.GetComponent<Rigidbody>().useGravity = true; // ACTIVA LA GRAVEDAD DEL ARMA
            armamento.gameObject.tag = "hierro"; // CAMBIA EL TAG DEL ARMA PARA SER REUTILIZABLE
            gameObject.SetActive(false); // MUERE EL ENEMIGO

        } // QUE OCURRE CUANDO EL PERSONAJE MUERE


        alerta = Physics.CheckSphere(transform.position, rangovision, aquienpersigo);  // VERIFICO SI JUGADOR ESTA EN EL RADIO DEL ENEMIGO

        if (alerta == true)  // SI EL ENEMIGO ESTA ALERTA ENTONCES..
        {
            Debug.DrawRay(transform.position, transform.forward * DistanciaRayo, Color.red);
            RaycastHit Rayohit;

            if (Physics.Raycast(transform.position,transform.forward, out Rayohit, DistanciaRayo, LayerMask.GetMask("Player")))
            {
                
                if (Rayohit.collider.name == "Player6")
                {
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(Player6.transform.position.x, transform.position.y, transform.position.z), Velocidadpersecución * Time.deltaTime);
                    Alguienenlaizquierda = true;
                }

                


                Rotar();
            }
        
                  
        }
    }



    private void Rotar()
    {
        if (Alguienenlaizquierda == false)
        {
            transform.eulerAngles = new Vector3(0, 90, 0);
        }




        if (Alguienenlaizquierda == true)
        {
            transform.eulerAngles = new Vector3(0, -90, 0);
        } 
    }

        public void Perseguir()
    {
        if (Intruso == 0) {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Player0.transform.position.x, transform.position.y, transform.position.z), Velocidadpersecución * Time.deltaTime);
        }
        if (Intruso == 1) {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(Player1.transform.position.x, transform.position.y, transform.position.z), Velocidadpersecución * Time.deltaTime);
        }
        if (Intruso == 2) { 
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(Player2.transform.position.x, transform.position.y, transform.position.z), Velocidadpersecución * Time.deltaTime); }
        if (Intruso == 3) {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Player3.transform.position.x, transform.position.y, transform.position.z), Velocidadpersecución * Time.deltaTime);
        }
        if (Intruso == 4) { 
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(Player4.transform.position.x, transform.position.y, transform.position.z), Velocidadpersecución * Time.deltaTime); }
        if (Intruso == 5) {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Player5.transform.position.x, transform.position.y, transform.position.z), Velocidadpersecución * Time.deltaTime);
        }
        if (Intruso == 6)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Player6.transform.position.x, transform.position.y, transform.position.z), Velocidadpersecución * Time.deltaTime);
        }
        

        
           
    }

    

    private void OnDrawGizmos()  //FUNCION PARA DIBUJAR LA VISION DEL ENEMIGO O EL RADIO, EN LA ESCENA
    {
        Gizmos.DrawWireSphere(transform.position,  rangovision);
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("hierro"))
        {
            if (Input.GetKey("q"))
            {
               // Gestordeaudio.instancia.ReproducirSonido("golpearreja");
                vida = vida - 10;
                Vector3 mover = new Vector3(1, 0, 1);
                gameObject.transform.position = gameObject.transform.position + mover;
            } // GOLPEAR ENEMIGO CON HIERRO

            if (other.gameObject.CompareTag("Player"))
            {

                {
                  
                    vida = 0;
                }

            } // ENEMIGO AFECTADO POR EXPLOSION

            
        }

        // COMPARAMOS QUE ENEMIGO ENTRO AL RANGO
        if (other.gameObject.CompareTag("Player0"))
            Intruso = 0;
        if (other.gameObject.CompareTag("Player1"))
            Intruso = 1;
        if (other.gameObject.CompareTag("Player2"))
            Intruso = 2;
        if (other.gameObject.CompareTag("Player3"))
            Intruso = 3;
        if (other.gameObject.CompareTag("Player4"))
            Intruso = 4;
        if (other.gameObject.CompareTag("Player5"))
            Intruso = 5;
        if (other.gameObject.CompareTag("Player6"))
            Intruso = 6;
    }
}

