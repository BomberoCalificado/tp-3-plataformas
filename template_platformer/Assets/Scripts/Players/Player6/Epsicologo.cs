using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Epsicologo : MonoBehaviour

{

    public LayerMask aquienpersigo;  // EN QUE CAPA VA DETECTAR OBJETOS
    public float rangovision; // RANGO DE VISION
    public bool alerta;  // ESTA ALERTA O NO?
    
    public float vida = 100; // VIDA DEL PERSONAJE
    public GameObject CampoDestructor;
    public float DistanciaRayo;

    // MOVIMIENTO PATRULLAJE
    public float Velocidad;   // VELOCIDAD DE PERSONAJE
    public float Distancia; // LARGO DE RAYO
    public GameObject Detector; // PUNTO DE INICIO DE RAYCAST
    public bool MovDerecha;
    private Rigidbody Rb; // RB DE PERSONAJE
    public RaycastHit Verificar; // ALMACENADOR DE RAYCAST
    public LayerMask Pisos; // CAPA PARA DETECTAR PISOS
    public bool Tocosuelo; // VERIFICACION PARA SABER SI TOCO EL SUELO




    void Start()
    {
        alerta = false;
        Rb = GetComponent<Rigidbody>();

    }


    void Update()
    {

        // MOVER
        Detectarpiso();
        Rb.velocity = new Vector3(Velocidad, Rb.velocity.y, Rb.velocity.z);

        
        

        
        if (vida <= 0)
        {
            Destroy(gameObject); // MUERE EL ENEMIGO

        } // QUE OCURRE CUANDO EL PERSONAJE MUERE


        alerta = Physics.CheckSphere(transform.position, rangovision, aquienpersigo);  // VERIFICO SI JUGADOR ESTA EN EL RADIO DEL ENEMIGO

        if (alerta == true)  // SI EL ENEMIGO ESTA ALERTA ENTONCES..
        {
            CampoDestructor.SetActive(true);
        }
        else { CampoDestructor.SetActive(false); }


    }

    private void Detectarpiso()
    {
        // VERIFICO SI EL OBJETO ESTA TOCANDO EL SUELO MEDIANTE RAYCAST
        Tocosuelo = Physics.Raycast(Detector.transform.position, Vector3.down, out Verificar, Distancia, Pisos);

        if (Tocosuelo == false)
        {
            Girar();
        }
    }
    private void Girar()
    {
        {
            MovDerecha = !MovDerecha;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
            Velocidad *= -1;

        }
    }

    private void OnDrawGizmos()  //FUNCION PARA DIBUJAR LA VISION DEL ENEMIGO O EL RADIO, EN LA ESCENA
    {
        Gizmos.DrawWireSphere(transform.position, rangovision);

        Gizmos.color = Color.red;

        Gizmos.DrawLine(Detector.transform.position, Detector.transform.position + Vector3.down * Distancia);

        }


}

