using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaPlayer6 : MonoBehaviour
    
{
    public float vidadepersonaje;
    public bool colision;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if(colision == true)
        {
            vidadepersonaje = vidadepersonaje - 10 * Time.deltaTime;
        }

        if (vidadepersonaje <= 0)
        {
            GameManager.gameOver = true;
            Destroy(gameObject);
        }
    }

        
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Lanza"))
        {

            colision = true;
            vidadepersonaje = vidadepersonaje - 50;
        }

        if (collision.gameObject.CompareTag("Esanguijuela"))
        {

            colision = true;
            vidadepersonaje = vidadepersonaje - 20;
        }

        if (collision.gameObject.CompareTag("Ebomba"))
        {
            colision = true;
            vidadepersonaje = vidadepersonaje - 100;
        }

        if (collision.gameObject.CompareTag("Campodefuerza"))
        {
            colision = true;
            
        }
    }
}
