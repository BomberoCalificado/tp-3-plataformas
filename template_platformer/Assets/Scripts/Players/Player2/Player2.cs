using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Player2 : MonoBehaviour
{
    public GameObject Plataforma1;
    public GameObject PlaraformaNaranja;
    public GameObject Boton1;
    public bool Subirplataforma;
    public bool Bajarplataforma;
    public float contador;
    public float ContadorBajar;
    private Vector3 Elevar = new Vector3(0, 2, 0);

    // disparar
    public GameObject Proyectiles;
    public GameObject InstanciadorP;
    public float velocidad;

    void Start()
    {
       
    }


    void Update()

    {   
        //if (Input.GetKeyDown(KeyCode.T))
        //{
        //    Disparar();
        //}
        

        if (Subirplataforma == true)
        {
            Plataforma1.transform.position = Plataforma1.transform.position + Elevar * Time.deltaTime;
            contador = contador - Time.deltaTime;
        }

        if (Bajarplataforma == true)
        {
            PlaraformaNaranja.transform.position = PlaraformaNaranja.transform.position - Elevar * Time.deltaTime;
            contador = contador - Time.deltaTime;
        }

        if (contador <= 0)
        {
            contador = 0;
            Subirplataforma = false;
            Bajarplataforma = false;
        }
    }

    //private void Disparar()
    //{
    //    Instantiate(Proyectiles, InstanciadorP.transform.position, Quaternion.identity);
    //    //Proyectiles.GetComponent<Rigidbody>().AddForce(transform.forward * velocidad * 3);
    //    Proyectiles.transform.position = Proyectiles.transform.position + new Vector3(3, 0, 0) * velocidad;
    //}
    
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Water"))
        {
            GameManager.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Boton1naranja"))
        {
            Subirplataforma = true;
            contador = 27;
         }

        if (collision.gameObject.CompareTag("BotonBajar"))
        {
            Bajarplataforma = true;
            contador = 18;
            EnemigoFinal.Alerta = true;
        }

        if (collision.gameObject.CompareTag("Ganar"))
        {
            Bajarplataforma = true;
            contador = 18;
            EnemigoFinal.Alerta = true;
        }

        if (collision.gameObject.CompareTag("Ganar"))
        {
            GameManager.Ganastejuego = true;
        }

    }
}
