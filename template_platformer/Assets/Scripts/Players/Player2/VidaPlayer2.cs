using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaPlayer2 : MonoBehaviour
{
    public static float vida;

    void Start()
    {
        vida = 150000;
    }

    void Update()
    {
        if (vida <= 0)
        {
            GameManager.gameOver = true;
        }

    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("EnemigoPatrullador"))
        {
            vida = vida - 30;
        }

        if (collision.gameObject.CompareTag("Esanguijuela"))
        {
            vida = vida - 15;
        }

        if (collision.gameObject.CompareTag("Ebomba"))
        {
            vida = vida - 40;
        }

        if (collision.gameObject.CompareTag("Campodefuerza"))
        {
            vida = vida - 1 * Time.deltaTime;
        }

        if (collision.gameObject.CompareTag("Bombasimple"))
        {
            vida = vida - 25;
        }

    }
}
