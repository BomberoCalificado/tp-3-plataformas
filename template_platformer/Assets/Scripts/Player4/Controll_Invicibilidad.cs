using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controll_Invicibilidad : MonoBehaviour
{
    Collider colliderToIgnore;
    Collider colliderToIgnore2;
    Collider colliderToIgnore3;
    Collider colliderToIgnore4;
    Collider colliderToIgnore5;
    Collider colliderToIgnore6;
    Collider colliderToIgnore7;
    Collider colliderToIgnore8;
    Collider colliderToIgnore9;
    Collider colliderToIgnore10;

    bool ignoreCollider = false;
    float ignoreTimer = 0.0f;
    float IgnoreDuration = 5.0f;

    void Start()
    {
        // Busca el collider por nombre
        colliderToIgnore = GameObject.Find("Plataforma1").GetComponent<Collider>();
        colliderToIgnore2 = GameObject.Find("Esanguijuela").GetComponent<Collider>();
        colliderToIgnore3 = GameObject.Find("Esanguijuela (2)").GetComponent<Collider>();
        colliderToIgnore4 = GameObject.Find("Esanguijuela (3)").GetComponent<Collider>();
        colliderToIgnore5 = GameObject.Find("Esanguijuela (4)").GetComponent<Collider>();
        colliderToIgnore6 = GameObject.Find("Esanguijuela (5)").GetComponent<Collider>();
        colliderToIgnore7 = GameObject.Find("Esanguijuela (6)").GetComponent<Collider>();
        colliderToIgnore8 = GameObject.Find("Esanguijuela (7)").GetComponent<Collider>();
        colliderToIgnore9 = GameObject.Find("Esanguijuela (8)").GetComponent<Collider>();
        colliderToIgnore10 = GameObject.Find("Esanguijuela (9)").GetComponent<Collider>();

    }

    void Update()
    {

        if (ignoreCollider && Time.time >= ignoreTimer)
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore, false);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore2, false);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore3, false);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore4, false);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore5, false);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore6, false);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore7, false);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore8, false);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore9, false);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore10, false);
            ignoreCollider = false;
        }
        // Si se presiona la tecla "Espacio", ignora el collider
        if (Input.GetKey(KeyCode.F) && !ignoreCollider)
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore, true);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore2, true);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore3, true);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore4, true);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore5, true);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore6, true);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore7, true);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore8, true);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore9, true);
            Physics.IgnoreCollision(GetComponent<Collider>(), colliderToIgnore10, true);

            ignoreCollider = true;
            ignoreTimer = Time.time + IgnoreDuration;
        }

    }
}