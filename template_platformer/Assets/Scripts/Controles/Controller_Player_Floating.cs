﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Floating : MonoBehaviour
{
    public float escalaInicial = 1f;
    public float escalaMaxima = 2f;
    public float escalaMinima = 0.5f;
    public float velocidadDeEscalado = 0.1f;
    public KeyCode teclaDeEscalado = KeyCode.M;

    private bool escalando = false;


    void Update()
    {
        if (Input.GetKeyDown(teclaDeEscalado))
        {
            escalando = true;
            velocidadDeEscalado = Mathf.Abs(velocidadDeEscalado);
        }
        else if (Input.GetKeyUp(teclaDeEscalado))
        {
            escalando = false;
        }

        if (escalando)
        {
            float nuevaEscala = transform.localScale.x + velocidadDeEscalado * Time.deltaTime;

            if (nuevaEscala > escalaMaxima || nuevaEscala < escalaMinima)
            {
                velocidadDeEscalado = -velocidadDeEscalado;
            }

            transform.localScale = new Vector3(nuevaEscala, nuevaEscala, nuevaEscala);
        }
    }

    private void Awake()
    {
        transform.localScale = new Vector3(escalaInicial, escalaInicial, escalaInicial);
    }
}