using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Esanguijuela : MonoBehaviour
{
    public bool Comiendosangre;
    
    public float vida;
    public float Velocidad;
    public bool Subir;
    public bool MovDerecha;
    private Rigidbody rb;

    public bool Tocosuelo;
    public Transform Detector;
    public RaycastHit Verificar;
    public float Distancia;
    public LayerMask Pisos;
    public bool Piesenlatierra;
    

    void Start()
    {
      rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector3(Velocidad, rb.velocity.y, rb.velocity.z);
       
        Detectarpiso();
        
    }

    private void FixedUpdate()
    {
        if (Piesenlatierra == true)
        {
            Saltar();
        }
    }

    private void Saltar()
    {
        Vector3 Salto = new Vector3(0, 12, 0);
        transform.position = transform.position + Salto * Time.deltaTime;

    }

    private void Detectarpiso()
    {
        // VERIFICO SI EL OBJETO ESTA TOCANDO EL SUELO MEDIANTE RAYCAST
        Tocosuelo = Physics.Raycast(Detector.transform.position, Vector3.down, out Verificar, Distancia, Pisos);

        if (Tocosuelo == false)
        {
            Girar();
        }
    }
    private void Girar()
    {
        {
            MovDerecha = !MovDerecha;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
            Velocidad *= -1;

        }
    }

    private void OnDrawGizmos() // DIBUJAMOS EL RAYCAST PARA POSICIONARLO BIEN
    {
        Gizmos.color = Color.red;

        Gizmos.DrawLine(Detector.transform.position, Detector.transform.position + Vector3.down * Distancia);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {

            Piesenlatierra = true;
            Saltar();
        }
        else { Piesenlatierra = false; }

        if (collision.gameObject.CompareTag("proyectil"))
        {

            Destroy(gameObject);
        }

        
    }

}
