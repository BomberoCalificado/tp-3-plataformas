using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Epatrullador : MonoBehaviour
{
    public float Vida;
    public float Velocidad;   // VELOCIDAD DE PERSONAJE
    public float Distancia; // LARGO DE RAYO
    public float DistanciaOjos; //
    public GameObject Detector; // PUNTO DE INICIO DE RAYCAST
    public GameObject Arma; //
    public bool MovDerecha;
    private Rigidbody Rb; // RB DE PERSONAJE
    public RaycastHit Verificar; // ALMACENADOR DE RAYCAST
    public RaycastHit Vista;
    public LayerMask Pisos; // CAPA PARA DETECTAR PISOS
    public LayerMask Players; // CAPA PARA DETECTAR PLAYERS    
    public bool VeoPlayer;
    public bool Tocosuelo; // VERIFICACION PARA SABER SI TOCO EL SUELO




    void Start()
    {   
        //REFERIENCIAMOS EL RB DEL OBJETO
        Rb = GetComponent<Rigidbody>();
        MovDerecha = true;
        
       
                
    }
    void Update()
    {
        // MOVER
        
        Rb.velocity = new Vector3(Velocidad, Rb.velocity.y, Rb.velocity.z);
        
        Detectarpiso();
        DetectarJugador();

        if (VeoPlayer == true && MovDerecha == true)
        {
            Velocidad = 10;
            
        }

        if (VeoPlayer == true && MovDerecha == false)
        {
            Velocidad = -10;
        }

        if (VeoPlayer == true)
            Arma.SetActive(true);
        else Arma.SetActive(false);




    }
    

    private void Detectarpiso()
    {
        // VERIFICO SI EL OBJETO ESTA TOCANDO EL SUELO MEDIANTE RAYCAST
        Tocosuelo = Physics.Raycast(Detector.transform.position, Vector3.down, out Verificar, Distancia, Pisos);

        if (Tocosuelo == false)
        {
            Girar();
        }
    }
    private void Girar()
    {
        {
            MovDerecha = !MovDerecha;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
            Velocidad *= -1;

        }
    }


    private void DetectarJugador()
    {
        if (MovDerecha == true)
        VeoPlayer = Physics.Raycast(Detector.transform.position, Vector3.right, out Vista, DistanciaOjos, Players);
        if (MovDerecha == false)
            VeoPlayer = Physics.Raycast(Detector.transform.position, Vector3.left, out Vista, DistanciaOjos, Players);

    }

    private void OnDrawGizmos() // DIBUJAMOS EL RAYCAST PARA POSICIONARLO BIEN
    { 
        Gizmos.color = Color.red;
        
        Gizmos.DrawLine(Detector.transform.position, Detector.transform.position + Vector3.down * Distancia);
        
        if (MovDerecha == true)
        Gizmos.DrawLine(Detector.transform.position, Detector.transform.position + Vector3.right * DistanciaOjos);
        if (MovDerecha == false)
            Gizmos.DrawLine(Detector.transform.position, Detector.transform.position + Vector3.left * DistanciaOjos);

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("proyectil"))
        {
            gameObject.SetActive(false);
        }
    }


}
