using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eboss : MonoBehaviour
{

    public float contador;
    public bool Arriba;
    public bool Bajar;
    public bool Izquierda;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        contador = contador + Time.deltaTime;

        if (contador > 3 && contador < 6)
            Arriba = true;

        if (contador > 6 && contador < 9)
            Bajar = true; Arriba = false;

        if (contador > 9 && contador < 12)
            Izquierda = true; Bajar = false; Arriba = false;
        

        if (contador == 12)
            contador = 0;
        Mover();
    }

    private void Mover()
    {
        if (contador <= 3) 
        {   Vector3 Mover = new Vector3(0, 1, 0);
            transform.position = transform.position + Mover * Time.deltaTime;
        } // subir
        
        if (Arriba == true)
        {
            Vector3 Mover = new Vector3(1, 0, 0);
            transform.position = transform.position + Mover * Time.deltaTime;
        } // mover a la derecha

        if (Bajar == true) // bajar
        {           
            Vector3 Mover = new Vector3(0, 1, 0);
            transform.position = transform.position - Mover * Time.deltaTime;
        } // bajar
        
        if (Izquierda == true) // izquierda
        {
            Vector3 Mover = new Vector3(1, 0, 0);
            transform.position = transform.position - Mover * Time.deltaTime;
        } // mover izquierda
    }
}
