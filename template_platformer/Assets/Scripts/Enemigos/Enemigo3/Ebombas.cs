using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ebombas : MonoBehaviour
{   
    public float contador;
    public bool Activar;
    public float velocidadziczac;
    public bool Tocosuelo;
    public Transform Detector;
    RaycastHit Verificar;
    public float Distancia;
    public LayerMask Pisos;
    public bool Movderecha;
   

    
    void Start()
    {
      
        Movderecha = true;
    }

   
    void Update()
    {
        contador = contador + Time.deltaTime;
        Detectarpiso();
        ZicZac();
        if( contador >= 2)
        {
            contador = 0;

        }
    }

    private void ZicZac()
    {

        if (Movderecha == true)
        {
            if (contador <= 1)
            {
                Vector3 Mover = new Vector3(1, 1, 0);
                transform.position = transform.position + Mover * velocidadziczac * Time.deltaTime;
            }

            if (contador > 1)
            {
                Vector3 Mover = new Vector3(-1, 1, 0);
                transform.position = transform.position - Mover * velocidadziczac * Time.deltaTime;
            }
        }

        if (Movderecha == false)
        {
            if (contador <= 1)
            {
                Vector3 Mover = new Vector3(1, 1, 0);
                transform.position = transform.position - Mover * velocidadziczac * Time.deltaTime;
            }

            if (contador > 1)
            {
                Vector3 Mover = new Vector3(-1, 1, 0);
                transform.position = transform.position + Mover * velocidadziczac * Time.deltaTime;
            }
        }

    }

    private void Explotar()
    {
        transform.localScale = transform.localScale * 2;
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Explotar();           
         }

        if (collision.gameObject.CompareTag("proyectil"))
        {
            Destroy(gameObject, 0);
        }
    }

    private void Detectarpiso()
    {
        // VERIFICO SI EL OBJETO ESTA TOCANDO EL SUELO MEDIANTE RAYCAST
        Tocosuelo = Physics.Raycast(Detector.transform.position, Vector3.right, out Verificar, Distancia, Pisos);

        if (Tocosuelo == true)
        {
            Girar();
        }
    }
    private void Girar()
    {
        {
            Movderecha = !Movderecha;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
            //Velocidad *= -1;

        }
    }

    private void OnDrawGizmos() // DIBUJAMOS EL RAYCAST PARA POSICIONARLO BIEN
    {
        Gizmos.color = Color.red;

        Gizmos.DrawLine(Detector.transform.position, Detector.transform.position + Vector3.right * Distancia);
    }

}
